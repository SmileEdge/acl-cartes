package com.ul.acl.model.game;

import com.ul.acl.model.cards.Card;
import com.ul.acl.model.cards.frenchcards.FrenchCard;
import com.ul.acl.model.cards.frenchcards.Rank;
import com.ul.acl.model.cards.frenchcards.Suits;
import com.ul.acl.model.players.BelotePlayer;

import java.io.*;
import java.util.Vector;

public class BeloteGame extends CardGame {

    public BeloteGame(BelotePlayer belotePlayer) {
        super("Belote",belotePlayer);

        Vector<Card> deck = new Vector<Card>();

        for (Suits s : Suits.values()) {
            for (Rank r : Rank.values()) {
                if(r == Rank.ACE){
                    FrenchCard c = new FrenchCard(s, r,11);
                    deck.add(c);
                }
                else if(r == Rank.KING){
                    FrenchCard c = new FrenchCard(s, r,4);
                    deck.add(c);
                }
                else if(r == Rank.QUEEN){
                    FrenchCard c = new FrenchCard(s, r,3);
                    deck.add(c);
                }
                else if(r == Rank.JACK){
                    FrenchCard c = new FrenchCard(s, r,2);
                    deck.add(c);
                }
                else if(r == Rank.TEN){
                    FrenchCard c = new FrenchCard(s, r,10);
                    deck.add(c);
                }
                else {
                    FrenchCard c = new FrenchCard(s, r,0);
                    deck.add(c);
                }
            }
        }

        this.deck=deck;
    }


    @Override
    public Card giveCard() {
       Card card = getRandomCard(this.deck);
       this.player.addCard(card);

       return card;
    }

    public void updateInGameScore(FrenchCard card1 , FrenchCard card2) {

        if(card1.getRank()!=card2.getRank() && card1.getSymbol()!=card2.getSymbol()){
            this.player.setScore(card1.getValue()+card2.getValue());
        }
        else if (card1.getRank()==card2.getRank()){
            this.player.setScore(-(card1.getValue()+card2.getValue()));
        }
        else if (card1.getRank()==card2.getRank() && card1.getSymbol()==card2.getSymbol()){
            this.player.setScore(2*(card1.getValue()+card2.getValue()));
        }
    }

    public BelotePlayer getPlayer(){
        return (BelotePlayer) this.player;
    }

    public void endGame(){

        try {
            File file = new File("./score.txt");
            if(getClass().getClassLoader().getResource("score.txt")==null) {
                PrintWriter writer = new PrintWriter("./score.txt", "UTF-8");
                writer.println(this.player.getName() + " " + this.player.getScore());
                writer.println("\n");
                writer.close();
            }
            else{
                //File file = new File("resources/score.txt");
                BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
                //bw.append(this.player.getName() + " " + this.player.getScore()+"\n");
                bw.append(this.player.getName()).append(' ').append(String.valueOf(this.player.getScore())).append('\n');
                bw.close();
            }
        }
        catch(Exception e){
            System.out.println("error while creating the score file");
        }
    }

}
