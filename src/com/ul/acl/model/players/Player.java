package com.ul.acl.model.players;

import com.ul.acl.model.cards.Card;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public abstract class Player {

    String name;
    int score;

    public Player(String name) {
        this.name = name;
        this.score = 0;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public abstract void addCard(Card card);

    public final IntegerProperty getScoreTable() {
        return new SimpleIntegerProperty(score);
    }

    public final StringProperty getNameTable() {
        return new SimpleStringProperty(name);
    }



    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }

}
