package com.ul.acl.model.cards.frenchcards;

public enum Rank {
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING,
    ACE;
}
