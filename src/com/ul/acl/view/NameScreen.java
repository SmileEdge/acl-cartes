package com.ul.acl.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class NameScreen {

    @FXML
    public Button validate;

    @FXML
    public TextField name ;

    public void validate(ActionEvent event)throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/GameScreen.fxml"));
        //Parent root1 = fxmlLoader.load(getClass().getResource("/fxml/GameScreen.fxml").openStream());
        Parent root1 = fxmlLoader.load();
        GameScreen control = fxmlLoader.getController();
        control.setBelotePlayerName(name.getText());
        Stage stage = new Stage();
        stage.setScene(new Scene(root1));
        stage.setResizable(false);
        stage.show();
        Stage stageActuel = (Stage) validate.getScene().getWindow();
        stageActuel.close();
    }

    public void close(ActionEvent event) throws IOException {
        Stage stageActuel = (Stage) validate.getScene().getWindow();
        stageActuel.close();
    }
}
