package com.ul.acl.view;

import com.ul.acl.model.cards.frenchcards.FrenchCard;
import com.ul.acl.model.game.BeloteGame;
import com.ul.acl.model.players.BelotePlayer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class GameScreen implements Initializable {

    @FXML
    public ImageView card1;

    @FXML
    public ImageView card2;

    @FXML
    public Button draw;

    @FXML
    public Text score;

    private int round = 0;

    private static BeloteGame beloteGame = new BeloteGame(new BelotePlayer("Giovanni"));

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setBelotePlayerName(String name){
        beloteGame.getPlayer().setName(name);
    }

    public void draw(ActionEvent event) throws IOException {


        if (round < 5) {

            FrenchCard tempCard1 = (FrenchCard) this.beloteGame.giveCard();
            String temp = tempCard1.getRank() + "_OF_" + tempCard1.getSymbol() + ".PNG";
            //System.out.println(temp);
            //System.out.println(getClass().getResource("/image/"));
            //Image img = new Image("com/ul/acl/resources/image/" + temp);
            Image img = new Image(getClass().getResource("/image/" + temp).toString());

            card1.setImage(img);


            FrenchCard tempCard2 = (FrenchCard) this.beloteGame.giveCard();
            temp = tempCard2.getRank() + "_OF_" + tempCard2.getSymbol() + ".PNG";
            img = new Image(getClass().getResource("/image/" + temp).toString());
            card2.setImage(img);

            this.beloteGame.updateInGameScore(tempCard1, tempCard2);

            this.score.setText(Integer.toString(beloteGame.getPlayer().getScore()));

            round++;
        } else {
            this.beloteGame.endGame();
            //Parent root1 = FXMLLoader.load(getClass().getResource("/fxml/ScoreScreen.fxml"));
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ScoreScreen.fxml"));
            //Parent root1 = fxmlLoader.load(getClass().getResource("/fxml/GameScreen.fxml").openStream());
            Parent root1 = fxmlLoader.load();
            ScoreScreen control = fxmlLoader.getController();
            control.createTable();
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setResizable(false);
            stage.show();
            Stage stageActuel = (Stage) score.getScene().getWindow();
            stageActuel.close();
        }


    }




}
