package com.ul.acl.view;
import com.ul.acl.model.players.BelotePlayer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.stream.Collectors;


public class ScoreScreen {

    @FXML
    public Button homeButton;

    @FXML
    private TableView<BelotePlayer> table;

    @FXML
    private TableColumn<BelotePlayer,String> player;

    @FXML
    private TableColumn<BelotePlayer, Integer> score;

    public void createTable(){
try {
    Collection<BelotePlayer> list = Files.readAllLines(new File("./score.txt").toPath())
            .stream()
            .map(line -> {
                String[] details = line.split(" ");
                BelotePlayer cd = new BelotePlayer(null);
                cd.setName(details[0]);
                System.out.println((details[0]));
                cd.setScore(Integer.parseInt(details[1]));
                System.out.println((details[1]));
                return cd;
            })
            .collect(Collectors.toList());

    ObservableList<BelotePlayer> details = FXCollections.observableArrayList(list);


    this.player.setCellValueFactory(data -> data.getValue().getNameTable());
    this.score.setCellValueFactory(data -> data.getValue().getScoreTable().asObject());
    this.score.setStyle( "-fx-alignment: CENTER;");

    this.table.setItems(details);

}
catch(Exception e){
    e.printStackTrace();
}



    }

    public void home(ActionEvent event)throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/StartScreen.fxml"));
        //Parent root1 = fxmlLoader.load(getClass().getResource("/fxml/GameScreen.fxml").openStream());
        Parent root1 = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root1));
        stage.setResizable(false);
        stage.show();
        Stage stageActuel = (Stage) homeButton.getScene().getWindow();
        stageActuel.close();
    }
}
