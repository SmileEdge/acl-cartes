package com.ul.acl;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/*public class Main {

    public static void main(String[] args) {
	// write your code here
        Game game = new BeloteGame(new BelotePlayer("Giovanni"));


        System.out.println(game.toString());

        for (int i=0; i<12 ; i++){
            System.out.println(((BeloteGame) game).giveCard().toString());
            System.out.println(game.getPlayer().toString());
        }




    }
}*/

public class Main extends Application {
    //Le main ne fait que lancer la fenetre de Demarrage et rien d'autre
    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/StartScreen.fxml"));
        Scene scene = new Scene(root);
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}